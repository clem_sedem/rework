Rails.application.routes.draw do
  devise_for :users
  resources :contacts
  get 'home/index'
  root to: 'home#index'

  resources :roles
  # resources :users
  resources :job_titles
  resources :localities
  resources :suburbs
  resources :cities
  resources :regions
  resources :contats


  devise_scope :user do
    get '/users/sign_up' => 'users#sign_in'
  end

   get '/users' => 'users#index'

  get '/users/new' => 'users#new', :as => 'new_user'
  post 'create_user' => 'users#create', as: :create_user
  get '/users/:id/edit' => 'users#edit', :as => 'edit_user'

  get '/users/:id' => 'users#show', :as => 'user'
  delete 'users/:id' => 'users#destroy'
  patch '/users/:id' => 'users#update', :as => 'update_user'

  get       'enable_contact'    =>  'contacts#enable_contact'
  get       'disable_contact'   =>  'contacts#disable_contact'

  get       'enable_region'    =>  'regions#enable_region'
  get       'disable_region'   =>  'regions#disable_region'

  get       'enable_city'    =>  'cities#enable_city'
  get       'disable_city'   =>  'cities#disable_city'

  get       'enable_suburb'    =>  'suburbs#enable_suburb'
  get       'disable_suburb'   =>  'suburbs#disable_suburb'

  get       'enable_locality'    =>  'localities#enable_locality'
  get       'disable_locality'   =>  'localities#disable_locality'

  get       'enable_job_title'    =>  'job_titles#enable_job_title'
  get       'disable_job_title'   =>  'job_titles#disable_job_title'

  get       'enable_user'    =>  'users#enable_user'
  get       'disable_user'   =>  'users#disable_user'

  get       'enable_role'    =>  'roles#enable_role'
  get       'disable_role'   =>  'roles#disable_role'
  
  get  'city_cascade' => 'suburbs#city_cascade'
  get 'suburb_cascade' => 'localities#suburb_cascade'
  get 'city_update' => 'suburbs#city_update'
  get 'suburb_update' => 'localities#suburb_update'
  get 'suburb_contact_cascade' => 'contacts#suburb_contact_cascade'
  get 'locality_contact_cascade' => 'contacts#locality_contact_cascade'

  # disabled users
  # get 'disabled' => 'users#sign_in'




  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
