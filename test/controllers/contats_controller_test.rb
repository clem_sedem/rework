require 'test_helper'

class ContatsControllerTest < ActionController::TestCase
  setup do
    @contat = contats(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:contats)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create contat" do
    assert_difference('Contat.count') do
      post :create, contat: { email: @contat.email, firstname: @contat.firstname, gender: @contat.gender, job_title_id: @contat.job_title_id, lastname: @contat.lastname, locality_id: @contat.locality_id, phone_number: @contat.phone_number }
    end

    assert_redirected_to contat_path(assigns(:contat))
  end

  test "should show contat" do
    get :show, id: @contat
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @contat
    assert_response :success
  end

  test "should update contat" do
    patch :update, id: @contat, contat: { email: @contat.email, firstname: @contat.firstname, gender: @contat.gender, job_title_id: @contat.job_title_id, lastname: @contat.lastname, locality_id: @contat.locality_id, phone_number: @contat.phone_number }
    assert_redirected_to contat_path(assigns(:contat))
  end

  test "should destroy contat" do
    assert_difference('Contat.count', -1) do
      delete :destroy, id: @contat
    end

    assert_redirected_to contats_path
  end
end
