json.extract! contact, :id, :firstname, :lastname, :phone_number, :email, :gender, :locality_id, :job_title_id, :created_at, :updated_at
json.url contact_url(contact, format: :json)
