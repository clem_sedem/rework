json.extract! suburb, :id, :city_id, :suburb_name, :created_at, :updated_at
json.url suburb_url(suburb, format: :json)
