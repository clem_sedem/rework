json.extract! job_title, :id, :job_title_name, :created_at, :updated_at
json.url job_title_url(job_title, format: :json)
