class City < ActiveRecord::Base
    validates :region_id, presence: true
    validates :city_name, presence: true

    belongs_to :region
    has_many :suburbs

    def self.to_csv
      CSV.generate do |csv|
        csv << column_names ## Header values of CSV
        all.each do |city|
          csv << city.attributes.values_at(*column_names) ##Row values of CSV
        end
      end
    end
     acts_as_xlsx


      #   def self.phone_number_search(phone_number)
      #    phone_number = "%#{phone_number}%"
      #    where('phone_number LIKE ?', phone_number)
      #  end

       def self.city_name_search(city_name)
        firstname = "%#{city_name}%"
        where('city_name LIKE ?', city_name)
      end


       def self.search_date(start = nil, ended = nil)
         if start && ended
           where(created_at: "#{start} 00:00:00".."#{ended} 23:59:59")
         else
           all
         end
       end



end
