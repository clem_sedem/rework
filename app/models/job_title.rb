class JobTitle < ActiveRecord::Base
  validates :job_title_name, presence: true

	has_one :contacts

  def self.to_csv
    CSV.generate do |csv|
      csv << column_names ## Header values of CSV
      all.each do |cont|
        csv << cont.attributes.values_at(*column_names) ##Row values of CSV
      end
    end
  end
   acts_as_xlsx


    #   def self.phone_number_search(phone_number)
    #    phone_number = "%#{phone_number}%"
    #    where('phone_number LIKE ?', phone_number)
    #  end

     def self.job_title_name_search(job_title_name)
      job_title_name = "%#{job_title_name}%"
      where('job_title_name LIKE ?', job_title_name)
    end


     def self.search_date(start = nil, ended = nil)
       if start && ended
         where(created_at: "#{start} 00:00:00".."#{ended} 23:59:59")
       else
         all
       end
     end

end
