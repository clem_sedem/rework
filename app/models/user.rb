class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

          # validates :username, presence: true
          # validates :email, email: true
          # validates :password, presence: true
          # validates :password_confirmation, presence: true
          # validates :role_id, presence: true

         belongs_to :role, class_name: "Role", foreign_key: :role_id

         def active_for_authentication?

            super && status?
         end

         def self.to_csv
           CSV.generate do |csv|
             csv << column_names ## Header values of CSV
             all.each do |cont|
               csv << cont.attributes.values_at(*column_names) ##Row values of CSV
             end
           end
         end
          acts_as_xlsx

            #
            #  def self.phone_number_search(phone_number)
            #   phone_number = "%#{phone_number}%"
            #   where('phone_number LIKE ?', phone_number)
            # end

            def self.username_search(username)
             username = "%#{username}%"
             where('username LIKE ?', username)
           end


            def self.search_date(start = nil, ended = nil)
              if start && ended
                where(created_at: "#{start} 00:00:00".."#{ended} 23:59:59")
              else
                all
              end
            end

end
