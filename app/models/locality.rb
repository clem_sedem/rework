class Locality < ActiveRecord::Base
   validates :suburb_id, presence: true
   validates :locality_name, presence: true

   has_many :contacts
   belongs_to :suburb

   def self.to_csv
     CSV.generate do |csv|
       csv << column_names ## Header values of CSV
       all.each do |loc|
         csv << loc.attributes.values_at(*column_names) ##Row values of CSV
       end
     end
   end
    acts_as_xlsx



      #  def self.phone_number_search(phone_number)
      #   phone_number = "%#{phone_number}%"
      #   where('phone_number LIKE ?', phone_number)
      # end

      def self.locality_name_search(locality_name)
       locality_name = "%#{locality_name}%"
       where('locality_name LIKE ?', locality_name)
     end


      def self.search_date(start = nil, ended = nil)
        if start && ended
          where(created_at: "#{start} 00:00:00".."#{ended} 23:59:59")
        else
          all
        end
      end

end
