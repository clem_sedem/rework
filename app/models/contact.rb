class Contact < ActiveRecord::Base
  validates :firstname, presence: true
	validates :lastname, presence: true
	validates :phone_number, presence: true
	validates :email, email: true
	validates :gender, presence: true
	validates :locality_id, presence: true
	validates :job_title_id, presence: true

	belongs_to :locality, foreign_key: :locality_id, class_name: "Locality"
	belongs_to :job_title

  def self.to_csv
    CSV.generate do |csv|
      csv << column_names ## Header values of CSV
      all.each do |cont|
        csv << cont.attributes.values_at(*column_names) ##Row values of CSV
      end
    end
  end
   acts_as_xlsx


   def self.phone_number_search(phone_number)
    phone_number = "%#{phone_number}%"
    where('phone_number LIKE ?', phone_number)
  end

  def self.firstname_search(firstname)
   firstname = "%#{firstname}%"
   where('firstname LIKE ?', firstname)
 end


  def self.search_date(start = nil, ended = nil)
    if start && ended
      where(created_at: "#{start} 00:00:00".."#{ended} 23:59:59")
    else
      all
    end
  end

end
