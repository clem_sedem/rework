class Region < ActiveRecord::Base
  validates :region_name, presence: true

	has_many :cities, class_name: "City", foreign_key: :region_id

  def self.to_csv
    CSV.generate do |csv|
      csv << column_names ## Header values of CSV
      all.each do |cont|
        csv << cont.attributes.values_at(*column_names) ##Row values of CSV
      end
    end
  end
   acts_as_xlsx


    #   def self.phone_number_search(phone_number)
    #    phone_number = "%#{region_name}%"
    #    where('region_name LIKE ?', phone_number)
    #  end

     def self.region_name_search(region_name)
      region_name = "%#{region_name}%"
      where('region_name LIKE ?', region_name)
    end


     def self.search_date(start = nil, ended = nil)
       if start && ended
         where(created_at: "#{start} 00:00:00".."#{ended} 23:59:59")
       else
         all
       end
     end

end
