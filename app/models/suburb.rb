class Suburb < ActiveRecord::Base
  validates :city_id, presence: true
	validates :suburb_name, presence: true

	belongs_to :city
	has_many :localities

  def self.to_csv
    CSV.generate do |csv|
      csv << column_names ## Header values of CSV
      all.each do |subs|
        csv << subs.attributes.values_at(*column_names) ##Row values of CSV
      end
    end
  end
   acts_as_xlsx


     #
    #   def self.phone_number_search(phone_number)
    #    phone_number = "%#{phone_number}%"
    #    where('phone_number LIKE ?', phone_number)
    #  end

     def self.suburb_name_search(suburb_name)
      suburb_name = "%#{suburb_name}%"
      where('suburb_name LIKE ?', suburb_name)
    end


     def self.search_date(start = nil, ended = nil)
       if start && ended
         where(created_at: "#{start} 00:00:00".."#{ended} 23:59:59")
       else
         all
       end
     end

end
