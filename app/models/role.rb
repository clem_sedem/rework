class Role < ActiveRecord::Base

   has_many :users

   def self.to_csv
     CSV.generate do |csv|
       csv << column_names ## Header values of CSV
       all.each do |cont|
         csv << cont.attributes.values_at(*column_names) ##Row values of CSV
       end
     end
   end
    acts_as_xlsx


      #  def self.phone_number_search(phone_number)
      #   phone_number = "%#{phone_number}%"
      #   where('phone_number LIKE ?', phone_number)
      # end

      def self.role_name_search(role_name)
       role_name = "%#{role_name}%"
       where('role_name LIKE ?', role_name)
     end


      def self.search_date(start = nil, ended = nil)
        if start && ended
          where(created_at: "#{start} 00:00:00".."#{ended} 23:59:59")
        else
          all
        end
      end

end
