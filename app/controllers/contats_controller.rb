class ContatsController < ApplicationController
  before_action :set_contat, only: [:show, :edit, :update, :destroy]

  # GET /contats
  # GET /contats.json
  def index
    @contats = Contat.all
  end

  # GET /contats/1
  # GET /contats/1.json
  def show
  end

  # GET /contats/new
  def new
    @contat = Contat.new
  end

  # GET /contats/1/edit
  def edit
  end

  # POST /contats
  # POST /contats.json
  def create
    @contat = Contat.new(contat_params)

    respond_to do |format|
      if @contat.save
        format.html { redirect_to @contat, notice: 'Contat was successfully created.' }
        format.json { render :show, status: :created, location: @contat }
      else
        format.html { render :new }
        format.json { render json: @contat.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /contats/1
  # PATCH/PUT /contats/1.json
  def update
    respond_to do |format|
      if @contat.update(contat_params)
        format.html { redirect_to @contat, notice: 'Contat was successfully updated.' }
        format.json { render :show, status: :ok, location: @contat }
      else
        format.html { render :edit }
        format.json { render json: @contat.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /contats/1
  # DELETE /contats/1.json
  def destroy
    @contat.destroy
    respond_to do |format|
      format.html { redirect_to contats_url, notice: 'Contat was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_contat
      @contat = Contat.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def contat_params
      params.require(:contat).permit(:firstname, :lastname, :phone_number, :email, :gender, :locality_id, :job_title_id)
    end
end
