class SuburbsController < ApplicationController
  before_action :set_suburb, only: [:show, :edit, :update, :destroy]

  # GET /suburbs
  # GET /suburbs.json
  def index
    @suburbs = Suburb.all.paginate(page: params[:page], per_page: 5).order('id ASC')
    @regions = Region.all.paginate(page: params[:page], per_page: 5).order('id ASC')
    @cities = City.all.paginate(page: params[:page], per_page: 5).order('id ASC')
    
        if params[:count]
         params[:count]
       else
         params[:count] = 30
       end
       if params[:page]
         page = params[:page].to_i
       else
         page = 1
       end
       if params[:per_page].present?
         perpage = params[:per_page]
       else
         perpage = 100
       end
       @per_page = params[:per_page] || Suburb.per_page || 30
       page = if params[:page]
                params[:page].to_i
              else
                1
               end
       per_page = 30
       offset = (page - 1) * per_page
       limit = page * per_page
       @array = *(offset...limit)

       if  params[:search_value] && params[:search_value].strip != ''
         if params[:search_param] == 'suburb_name'
           @suburbs = Suburb.suburb_name_search(params[:search_value].strip).paginate(page: params[:page], per_page: params[:count]).order('id asc')
          elsif params[:search_param] == 'phone_number'
            logger.info "We are INSIDE THE NUMBER PARAM"
            @suburbs = Suburb.phone_number_search(params[:search_value].strip).paginate(page: params[:page], per_page: params[:count]).order('id asc')
         elsif params[:search_param] == 'date'
           start = (params["start_date"] + " " + "0:00:00")# Time.zone.parse(params["start_date"].to_s + " " + "0:00:00").utc # params["start_date"].to_s + "0:00:00"
           ended = params["end_date"] + " " + ("23:59:59") # Time.zone.parse(params["end_date"].to_s + " " + "23:59:59").utc # params["end_date"].to_s + "23:59:59"
           @suburbs =Suburb.search_date(start,ended).paginate(page: params[:page], per_page: params[:count]).order('id asc')

       else
          @suburbs = Suburb.paginate(page: params[:page], per_page: params[:count]).order('id desc')
          @search_json = []
         end
       end
       p "JSON ARRAY: #{@search_json}"



    respond_to do |format|
     format.html
     format.xlsx
     format.js
     format.csv { send_data @suburbs.to_csv}
   end
  end

  # GET /suburbs/1
  # GET /suburbs/1.json
  def show
  end

  def enable_suburb
      suburb_id=params[:id]
     if Suburb.update(suburb_id, :status => 1)
       redirect_to suburbs_path, remote: true, notice: 'Suburb was successfully enabled.'
     end
  end

  def disable_suburb
      suburb_id=params[:id]
     if Suburb.update(suburb_id, :status => 0)
       redirect_to suburbs_path, remote: true, notice: 'Suburb was successfully disabled.'
     end
  end

  # GET /suburbs/new
  def new
    # @suburbs = Suburb.all
    @cities = City.where('status = true' )
    @suburb = Suburb.new
    @regions = Region.all.order('region_name ASC')
  end

  # GET /suburbs/1/edit
  def edit
    # @suburbs = Suburb.all
    @cities = City.where('status = true' )
    @regions = Region.all.order('region_name ASC')

    @region_id = @suburb.city.region_id 
    logger.info "region id: #{@region_id}"
  end

  # POST /suburbs
  # POST /suburbs.json
  def create
    # @suburbs = Suburb.all
    @cities = City.where('status = true' )
    @suburb = Suburb.new(suburb_params)

    respond_to do |format|
      if @suburb.save
        format.html { redirect_to @suburb, notice: 'Suburb was successfully created.' }
        format.json { render :show, status: :created, location: @suburb }
        flash[:notice] = 'Suburb was successfully created.'
        format.js 
      else
        format.html { render :new }
        format.json { render json: @suburb.errors, status: :unprocessable_entity }
        format.js {render :new}
      end
    end
  end

  def city_cascade
      region = Region.find(params[:region_id])
      logger.info "this is the region: #{region.inspect}"

      logger.info "cities in region: #{region.cities.inspect}"
      @cities = region.cities.map{|a| [a.city_name, a.id]}.insert(0, "Choose a city")
    end

    def city_update
      region = Region.find(params[:region_id])
      # logger.info "this is the region: #{region.inspect}"

      # logger.info "cities in region: #{region.cities.inspect}"
      @cities = region.cities.map{|a| [a.city_name, a.id]}.insert(1)
    end

    

  # PATCH/PUT /suburbs/1
  # PATCH/PUT /suburbs/1.json
  def update
    # @suburbs = Suburb.all
    @cities = City.where('status = true' )
    respond_to do |format|
      if @suburb.update(suburb_params)
        format.html { redirect_to @suburb, notice: 'Suburb was successfully updated.' }
        format.json { render :show, status: :ok, location: @suburb }
        format.js
      else
        format.html { render :edit }
        format.json { render json: @suburb.errors, status: :unprocessable_entity }
        format.js {render :edit}
      end
    end
  end

  # DELETE /suburbs/1
  # DELETE /suburbs/1.json
  def destroy
    @suburb.destroy
    respond_to do |format|
      format.html { redirect_to suburbs_url, notice: 'Suburb was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_suburb
      @suburb = Suburb.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def suburb_params
      params.require(:suburb).permit(:city_id, :suburb_name, :status)
    end
end
