class LocalitiesController < ApplicationController
  before_action :set_locality, only: [:show, :edit, :update, :destroy]

  # GET /localities
  # GET /localities.json
  def index
    @localities = Locality.all.paginate(page: params[:page], per_page: 8)


        if params[:count]
         params[:count]
       else
         params[:count] = 30
       end
       if params[:page]
         page = params[:page].to_i
       else
         page = 1
       end
       if params[:per_page].present?
         perpage = params[:per_page]
       else
         perpage = 100
       end
       @per_page = params[:per_page] || Locality.per_page || 30
       page = if params[:page]
                params[:page].to_i
              else
                1
               end
       per_page = 30
       offset = (page - 1) * per_page
       limit = page * per_page
       @array = *(offset...limit)

       if  params[:search_value] && params[:search_value].strip != ''
         if params[:search_param] == 'locality_name'
           @localities = Locality.locality_name_search(params[:search_value].strip).paginate(page: params[:page], per_page: params[:count]).order('id asc')
          elsif params[:search_param] == 'phone_number'
            logger.info "We are INSIDE THE NUMBER PARAM"
            @localities = Locality.phone_number_search(params[:search_value].strip).paginate(page: params[:page], per_page: params[:count]).order('id asc')
         elsif params[:search_param] == 'date'
           start = (params["start_date"] + " " + "0:00:00")# Time.zone.parse(params["start_date"].to_s + " " + "0:00:00").utc # params["start_date"].to_s + "0:00:00"
           ended = params["end_date"] + " " + ("23:59:59") # Time.zone.parse(params["end_date"].to_s + " " + "23:59:59").utc # params["end_date"].to_s + "23:59:59"
          @localities =Locality.search_date(start,ended).paginate(page: params[:page], per_page: params[:count]).order('id asc')

       else
          @contacts = Locality.paginate(page: params[:page], per_page: params[:count]).order('id desc')
          @search_json = []
         end
       end
       p "JSON ARRAY: #{@search_json}"




    respond_to do |format|
     format.html
     format.xlsx
     format.js
     format.csv { send_data @localities.to_csv}
   end
  end

  # GET /localities/1
  # GET /localities/1.json
  def show
  end

  def enable_locality
      locality_id=params[:id]
     if Locality.update(locality_id, :status => 1)
       redirect_to localities_path, notice: 'Locality was successfully enabled.'
     end
  end

  def disable_locality
      locality_id=params[:id]
     if Locality.update(locality_id, :status => 0)
       redirect_to localities_path, notice: 'Locality was successfully disabled.'
     end
  end

  # GET /localities/new
  def new
    @suburbs = Suburb.where('status = true' )
    @cities = City.all.order('id ASC')
    @regions = Region.all.order('id ASC')
    @locality = Locality.new
  end

  # GET /localities/1/edit
  def edit
    @suburbs = Suburb.where('status = true' )
    @cities = City.all.order('city_name ASC')
    @regions = Region.all.order('region_name ASC')

    @region_id = @locality.suburb.city.region_id 
    logger.info "region id: #{@region_id}"

    # logger.info "suburb: #{@locality.suburb.inspect}" 
    # logger.info "city:  #{@locality.suburb.city.inspect}" 
    # logger.info "region: #{@locality.suburb.city.region.inspect}"
  end

  # POST /localities
  # POST /localities.json
  def create
    @suburbs = Suburb.where('status = true' )
    @locality = Locality.new(locality_params)

    respond_to do |format|
      if @locality.save
        format.html { redirect_to @locality, notice: 'Locality was successfully created.' }
        format.json { render :show, status: :created, location: @locality }
        format.js
      else
        format.js { render :new }
        format.html { render :new }
        format.json { render json: @locality.errors, status: :unprocessable_entity }
      end
    end
  end

  def city_cascade
      region = Region.find(params[:region_id])
      # logger.info "this is the region: #{region.inspect}"

      # logger.info "cities in region: #{region.cities.inspect}"
      @cities = region.cities.map{|a| [a.city_name, a.id]}.insert(0, "Choose a city")
    end

    def suburb_cascade
      city = City.find(params[:city_id])
      # logger.info "this is the region: #{city.inspect}"

      # logger.info "cities in region: #{city.suburbs.inspect}"
      @suburbs = city.suburbs.map{|b| [b.suburb_name, b.id]}.insert(0, "Choose a suburb")
    end

    def suburb_update
      city = City.find(params[:city_id])
      @suburbs = city.suburbs.map{|b| [b.suburb_name, b.id]}.insert(1)
    end


  # PATCH/PUT /localities/1
  # PATCH/PUT /localities/1.json
  def update
    @suburbs = Suburb.where('status = true' )
    @regions = Region.all.order('id ASC')
    @cities = City.where('status = true')
    respond_to do |format|
      if @locality.update(locality_params)
        format.html { redirect_to @locality, notice: 'Locality was successfully updated.' }
        format.json { render :show, status: :ok, location: @locality }
        format.js
      else
        format.html { render :edit }
        format.json { render json: @locality.errors, status: :unprocessable_entity }
        format.js{render :edit}
      end
    end
  end

  # DELETE /localities/1
  # DELETE /localities/1.json
  def destroy
    @locality.destroy
    respond_to do |format|
      format.html { redirect_to localities_url, notice: 'Locality was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_locality
      @locality = Locality.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def locality_params
      params.require(:locality).permit(:suburb_id, :locality_name, :status)
    end
end
