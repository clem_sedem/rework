class ApplicationController < ActionController::Base
  require 'CSV'
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  before_action :authenticate_user!
  before_filter :authenticate_user!

 add_flash_types :success, :warning, :danger, :info

 # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

 before_filter :configure_permitted_parameters, if: :devise_controller?

 protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:sign_up) { |u| u.permit(:username, :fullname, :email, :password, :password_confirmation, :user_type_id,:creator_id, :remember_me) }
    devise_parameter_sanitizer.for(:sign_in) { |u| u.permit(:username,:login, :password, :user_type_id,:creator_id,:remember_me) }
    devise_parameter_sanitizer.for(:account_update) { |u| u.permit(:username, :fullname, :email, :password, :password_confirmation, :current_password,:user_type_id,:entity_master_id,:creator_id,) }
  end

end
