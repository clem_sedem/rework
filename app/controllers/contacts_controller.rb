class ContactsController < ApplicationController
  before_action :set_contact, only: [:show, :edit, :update, :destroy]

  # GET /contacts
  # GET /contacts.json
  def index
    @contacts = Contact.all.paginate(page: params[:page], per_page: 8).order('id ASC')

    if params[:count]
     params[:count]
   else
     params[:count] = 30
   end
   if params[:page]
     page = params[:page].to_i
   else
     page = 1
   end
   if params[:per_page].present?
     perpage = params[:per_page]
   else
     perpage = 100
   end
   @per_page = params[:per_page] || Contact.per_page || 30
   page = if params[:page]
            params[:page].to_i
          else
            1
           end
   per_page = 30
   offset = (page - 1) * per_page
   limit = page * per_page
   @array = *(offset...limit)

   if  params[:search_value] && params[:search_value].strip != ''
     if params[:search_param] == 'firstname'
       @contacts = Contact.firstname_search(params[:search_value].strip).paginate(page: params[:page], per_page: params[:count]).order('id asc')
      elsif params[:search_param] == 'phone_number'
        logger.info "We are INSIDE THE NUMBER PARAM"
        @contacts = Contact.phone_number_search(params[:search_value].strip).paginate(page: params[:page], per_page: params[:count]).order('id asc')
     elsif params[:search_param] == 'date'
       start = (params["start_date"] + " " + "0:00:00")# Time.zone.parse(params["start_date"].to_s + " " + "0:00:00").utc # params["start_date"].to_s + "0:00:00"
       ended = params["end_date"] + " " + ("23:59:59") # Time.zone.parse(params["end_date"].to_s + " " + "23:59:59").utc # params["end_date"].to_s + "23:59:59"
       @contacts =
         Contact.search_date(start,ended).paginate(page: params[:page], per_page: params[:count]).order('id asc')

   else
      @contacts = Contact.paginate(page: params[:page], per_page: params[:count]).order('id desc')
      @search_json = []
     end
   end
   p "JSON ARRAY: #{@search_json}"



    respond_to do |format|
     format.html
     format.xlsx
     format.csv { send_data @contacts.to_csv}
   end

  end

  # GET /contacts/1
  # GET /contacts/1.json
  def show
  end

  def suburb_contact_cascade
    city = City.find(params[:city_id])
      # logger.info "this is the region: #{city.inspect}"

      # logger.info "cities in region: #{city.suburbs.inspect}"
    @suburbs = city.suburbs.map{|b| [b.suburb_name, b.id]}.insert(0, "Choose a suburb")
  end

  def locality_contact_cascade
    suburb = Suburb.find(params[:suburb_id])
      # logger.info "this is the region: #{city.inspect}"

      # logger.info "cities in region: #{city.suburbs.inspect}"
    @localities = suburb.localities.map{|b| [b.locality_name, b.id]}.insert(0, "Choose a locality")
  end

 def enable_contact
     contact_id=params[:id]
    if Contact.update(contact_id, :status => 1)
      redirect_to contacts_path, notice: 'Contact was successfully enabled.'
    end
 end

 def disable_contact
   contact_id=params[:id]
  if Contact.update(contact_id, :status => 0)
    redirect_to contacts_path, notice: 'Contact was successfully disabled.'
  end
 end


  # GET /contacts/new
  def new
    @localities = Locality.where('status = true' )
    @job_titles = JobTitle.where('status = true' )
    @regions = Region.all
    @contact = Contact.new
  end

  # GET /contacts/1/edit
  def edit
    @localities = Locality.where('status = true' )
    @job_titles = JobTitle.where('status = true' )
  end

  # POST /contacts
  # POST /contacts.json
  def create
    @localities = Locality.where('status = true' )
    @job_titles = JobTitle.where('status = true' )
    @contact = Contact.new(contact_params)

    respond_to do |format|
      if @contact.save
        format.html { redirect_to @contact, notice: 'Contact was successfully created.' }
        format.json { render :show, status: :created, location: @contact }
      else
        format.html { render :new }
        format.json { render json: @contact.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /contacts/1
  # PATCH/PUT /contacts/1.json
  def update
    @localities = Locality.where('status = true' )
    @job_titles = JobTitle.where('status = true' )
    respond_to do |format|
      if @contact.update(contact_params)
        format.html { redirect_to @contact, notice: 'Contact was successfully updated.' }
        format.json { render :show, status: :ok, location: @contact }
      else
        format.html { render :edit }
        format.json { render json: @contact.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /contacts/1
  # DELETE /contacts/1.json
  def destroy
    @contact.destroy
    respond_to do |format|
      format.html { redirect_to contacts_url, notice: 'Contact was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_contact
      @contact = Contact.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def contact_params
      params.require(:contact).permit(:firstname, :lastname, :phone_number, :email, :gender, :locality_id, :job_title_id, :status)
    end
end
