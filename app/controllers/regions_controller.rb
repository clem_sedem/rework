class RegionsController < ApplicationController
  before_action :set_region, only: [:show, :edit, :update, :destroy]

  # GET /regions
  # GET /regions.json
  def index
    @regions = Region.all.paginate(page: params[:page], per_page: 5).order('id ASC')
    @cities = City.all.paginate(page: params[:page], per_page: 5).order('id ASC')
    @suburbs = Suburb.all.paginate(page: params[:page], per_page: 5).order('id ASC')
    @localities = Locality.all.paginate(page: params[:page], per_page: 5).order('id ASC')

        if params[:count]
         params[:count]
       else
         params[:count] = 30
       end
       if params[:page]
         page = params[:page].to_i
       else
         page = 1
       end
       if params[:per_page].present?
         perpage = params[:per_page]
       else
         perpage = 100
       end
       @per_page = params[:per_page] || Region.per_page || 30
       page = if params[:page]
                params[:page].to_i
              else
                1
               end
       per_page = 30
       offset = (page - 1) * per_page
       limit = page * per_page
       @array = *(offset...limit)

       if  params[:search_value] && params[:search_value].strip != ''
         if params[:search_param] == 'region_name'
           @regions = Region.region_name_search(params[:search_value].strip).paginate(page: params[:page], per_page: params[:count]).order('id asc')
          elsif params[:search_param] == 'phone_number'
            logger.info "We are INSIDE THE NUMBER PARAM"
            @Regions = Region.phone_number_search(params[:search_value].strip).paginate(page: params[:page], per_page: params[:count]).order('id asc')
         elsif params[:search_param] == 'date'
           start = (params["start_date"] + " " + "0:00:00")# Time.zone.parse(params["start_date"].to_s + " " + "0:00:00").utc # params["start_date"].to_s + "0:00:00"
           ended = params["end_date"] + " " + ("23:59:59") # Time.zone.parse(params["end_date"].to_s + " " + "23:59:59").utc # params["end_date"].to_s + "23:59:59"
           @regions =
             Region.search_date(start,ended).paginate(page: params[:page], per_page: params[:count]).order('id asc')

       else
          @regions = Region.paginate(page: params[:page], per_page: params[:count]).order('id desc')
          @search_json = []
         end
       end
       p "JSON ARRAY: #{@search_json}"




    respond_to do |format|
     format.html
     format.xlsx
     format.csv { send_data @regions.to_csv}
   end

  end

  # GET /regions/1
  # GET /regions/1.json
  def show
    @regions = Region.find(params[:id])
    @suburbs = Suburb.all
    @localities = Locality.all
  end

  def enable_region
      region_id=params[:id]
     if Region.update(region_id, :status => 1)
       redirect_to regions_path, notice: 'Region was successfully enabled.'
     end
  end

  def disable_region
      region_id=params[:id]
     if Region.update(region_id, :status => 0)
       redirect_to regions_path, notice: 'Region was successfully disabled.'
     end
  end



  # GET /regions/new
  def new
    @region = Region.new
  end

  # GET /regions/1/edit
  def edit
    
  end

  # POST /regions
  # POST /regions.json
  def create
    @region = Region.new(region_params)

    respond_to do |format|
      if @region.save
        format.js
        format.html { redirect_to @region, notice: 'Region was successfully created.' }
        format.json { render :show, status: :created, location: @region }
      else
        format.js {render :new}
        format.html { render :new }
        format.json { render json: @region.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /regions/1
  # PATCH/PUT /regions/1.json
  def update
    respond_to do |format|
      if @region.update(region_params)
        format.html { redirect_to @region, notice: 'Region was successfully updated.' }
        format.json { render :show, status: :ok, location: @region }
      else
        format.html { render :edit }
        format.json { render json: @region.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /regions/1
  # DELETE /regions/1.json
  def destroy
    @region.destroy
    respond_to do |format|
      format.html { redirect_to regions_url, notice: 'Region was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_region
      @region = Region.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def region_params
      params.require(:region).permit(:region_name, :status)
    end
end
