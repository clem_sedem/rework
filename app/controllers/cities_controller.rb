class CitiesController < ApplicationController
  before_action :set_city, only: [:show, :edit, :update, :destroy]

  # GET /cities
  # GET /cities.json
  def index
     @cities = City.all.paginate(page: params[:page], per_page: 5).order('id ASC')
     
        if params[:count]
         params[:count]
       else
         params[:count] = 30
       end
       if params[:page]
         page = params[:page].to_i
       else
         page = 1
       end
       if params[:per_page].present?
         perpage = params[:per_page]
       else
         perpage = 100
       end
       @per_page = params[:per_page] || Contact.per_page || 30
       page = if params[:page]
                params[:page].to_i
              else
                1
               end
       per_page = 30
       offset = (page - 1) * per_page
       limit = page * per_page
       @array = *(offset...limit)

       if  params[:search_value] && params[:search_value].strip != ''
         if params[:search_param] == 'city_name'
           @cities = City.city_name_search(params[:search_value].strip).paginate(page: params[:page], per_page: params[:count]).order('id asc')
          elsif params[:search_param] == 'phone_number'
            logger.info "We are INSIDE THE NUMBER PARAM"
            @cities = City.phone_number_search(params[:search_value].strip).paginate(page: params[:page], per_page: params[:count]).order('id asc')
         elsif params[:search_param] == 'date'
           start = (params["start_date"] + " " + "0:00:00")# Time.zone.parse(params["start_date"].to_s + " " + "0:00:00").utc # params["start_date"].to_s + "0:00:00"
           ended = params["end_date"] + " " + ("23:59:59") # Time.zone.parse(params["end_date"].to_s + " " + "23:59:59").utc # params["end_date"].to_s + "23:59:59"
           @cities =
             City.search_date(start,ended).paginate(page: params[:page], per_page: params[:count]).order('id asc')

       else
          @cities = City.paginate(page: params[:page], per_page: params[:count]).order('id desc')
          @search_json = []
         end
       end
       p "JSON ARRAY: #{@search_json}"



    respond_to do |format|
     format.html
     format.xlsx
     format.js
     format.csv { send_data @cities.to_csv}
   end

  end

  # GET /cities/1
  # GET /cities/1.json
  def show
  end

  def enable_city
      city_id=params[:id]
     if City.update(city_id, :status => 1)
       redirect_to cities_path, notice: 'City was successfully enabled.'
     end
  end

  def disable_city
      city_id=params[:id]
     if City.update(city_id, :status => 0)
       redirect_to cities_path, notice: 'City was successfully disabled.'
     end
  end



  # GET /cities/new
  def new
    @regions = Region.where('status = true' )
    @city = City.new
  end

  # GET /cities/1/edit
  def edit
    @regions = Region.where('status = true' )
  end

  # POST /cities
  # POST /cities.json
  def create
    @regions = Region.where('status = true' )
    @city = City.new(city_params)

    respond_to do |format|
      if @city.save
        format.html { redirect_to @city, notice: 'City was successfully created.' }
        format.json { render :show, status: :created, location: @city }
        format.js
      else
        format.html { render :new }
        format.js { render :new }
        format.json { render json: @city.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /cities/1
  # PATCH/PUT /cities/1.json
  def update
    @regions = Region.where('status = true' )
    respond_to do |format|
      if @city.update(city_params)
        format.html { redirect_to @city, notice: 'City was successfully updated.' }
        format.json { render :show, status: :ok, location: @city }
        format.js 

      else
        format.html { render :edit }
        format.json { render json: @city.errors, status: :unprocessable_entity }
        format.js {render :edit}
      end
    end
  end

  # DELETE /cities/1
  # DELETE /cities/1.json
  def destroy
    @city.destroy
    respond_to do |format|
      format.html { redirect_to cities_url, notice: 'City was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_city
      @city = City.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def city_params
      params.require(:city).permit(:region_id, :city_name, :status)
    end
end
