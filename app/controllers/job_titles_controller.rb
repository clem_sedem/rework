class JobTitlesController < ApplicationController
  before_action :set_job_title, only: [:show, :edit, :update, :destroy]

  # GET /job_titles
  # GET /job_titles.json
  def index
    @job_titles = JobTitle.all.paginate(page: params[:page], per_page: 6)


        if params[:count]
         params[:count]
       else
         params[:count] = 30
       end
       if params[:page]
         page = params[:page].to_i
       else
         page = 1
       end
       if params[:per_page].present?
         perpage = params[:per_page]
       else
         perpage = 100
       end
       @per_page = params[:per_page] || JobTitle.per_page || 30
       page = if params[:page]
                params[:page].to_i
              else
                1
               end
       per_page = 30
       offset = (page - 1) * per_page
       limit = page * per_page
       @array = *(offset...limit)

       if  params[:search_value] && params[:search_value].strip != ''
         if params[:search_param] == 'job_title_name'
           @job_titles = JobTitle.job_title_name_search(params[:search_value].strip).paginate(page: params[:page], per_page: params[:count]).order('id asc')
          elsif params[:search_param] == 'phone_number'
            logger.info "We are INSIDE THE NUMBER PARAM"
            @job_titles = JobTitle.phone_number_search(params[:search_value].strip).paginate(page: params[:page], per_page: params[:count]).order('id asc')
         elsif params[:search_param] == 'date'
           start = (params["start_date"] + " " + "0:00:00")# Time.zone.parse(params["start_date"].to_s + " " + "0:00:00").utc # params["start_date"].to_s + "0:00:00"
           ended = params["end_date"] + " " + ("23:59:59") # Time.zone.parse(params["end_date"].to_s + " " + "23:59:59").utc # params["end_date"].to_s + "23:59:59"
           @job_titles =JobTitle.search_date(start,ended).paginate(page: params[:page], per_page: params[:count]).order('id asc')

       else
          @job_titles = JobTitle.paginate(page: params[:page], per_page: params[:count]).order('id desc')
          @search_json = []
         end
       end
       p "JSON ARRAY: #{@search_json}"



    respond_to do |format|
     format.html
     format.xlsx
     format.csv { send_data @job_titles.to_csv}
   end
  end

  # GET /job_titles/1
  # GET /job_titles/1.json
  def show
  end

  def enable_job_title
      job_title_id=params[:id]
     if JobTitle.update(job_title_id, :status => 1)
       redirect_to job_titles_path, notice: 'Job title was successfully enabled.'
     end
  end

  def disable_job_title
      job_title_id=params[:id]
     if JobTitle.update(job_title_id, :status => 0)
       redirect_to job_titles_path, notice: 'Job title was successfully disabled.'
     end
  end

  # GET /job_titles/new
  def new
    @job_title = JobTitle.new
  end

  # GET /job_titles/1/edit
  def edit
  end

  # POST /job_titles
  # POST /job_titles.json
  def create
    @job_title = JobTitle.new(job_title_params)

    respond_to do |format|
      if @job_title.save
        format.html { redirect_to @job_title, notice: 'Job title was successfully created.' }
        format.json { render :show, status: :created, location: @job_title }
      else
        format.html { render :new }
        format.json { render json: @job_title.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /job_titles/1
  # PATCH/PUT /job_titles/1.json
  def update
    respond_to do |format|
      if @job_title.update(job_title_params)
        format.html { redirect_to @job_title, notice: 'Job title was successfully updated.' }
        format.json { render :show, status: :ok, location: @job_title }
      else
        format.html { render :edit }
        format.json { render json: @job_title.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /job_titles/1
  # DELETE /job_titles/1.json
  def destroy
    @job_title.destroy
    respond_to do |format|
      format.html { redirect_to job_titles_url, notice: 'Job title was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_job_title
      @job_title = JobTitle.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def job_title_params
      params.require(:job_title).permit(:job_title_name, :status)
    end
end
