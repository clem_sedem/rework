class RolesController < ApplicationController
  before_action :set_role, only: [:show, :edit, :update, :destroy]

  # GET /roles
  # GET /roles.json
  def index
    @roles = Role.all.paginate(page: params[:page], per_page: 8)


        if params[:count]
         params[:count]
       else
         params[:count] = 30
       end
       if params[:page]
         page = params[:page].to_i
       else
         page = 1
       end
       if params[:per_page].present?
         perpage = params[:per_page]
       else
         perpage = 100
       end
       @per_page = params[:per_page] || Role.per_page || 30
       page = if params[:page]
                params[:page].to_i
              else
                1
               end
       per_page = 30
       offset = (page - 1) * per_page
       limit = page * per_page
       @array = *(offset...limit)

       if  params[:search_value] && params[:search_value].strip != ''
         if params[:search_param] == 'role_name'
           @roles = Role.role_name_search(params[:search_value].strip).paginate(page: params[:page], per_page: params[:count]).order('id asc')
          elsif params[:search_param] == 'phone_number'
            logger.info "We are INSIDE THE NUMBER PARAM"
            @roles = Role.phone_number_search(params[:search_value].strip).paginate(page: params[:page], per_page: params[:count]).order('id asc')
         elsif params[:search_param] == 'date'
           start = (params["start_date"] + " " + "0:00:00")# Time.zone.parse(params["start_date"].to_s + " " + "0:00:00").utc # params["start_date"].to_s + "0:00:00"
           ended = params["end_date"] + " " + ("23:59:59") # Time.zone.parse(params["end_date"].to_s + " " + "23:59:59").utc # params["end_date"].to_s + "23:59:59"
           @roles =Role.search_date(start,ended).paginate(page: params[:page], per_page: params[:count]).order('id asc')

       else
          @roles = Role.paginate(page: params[:page], per_page: params[:count]).order('id desc')
          @search_json = []
         end
       end
       p "JSON ARRAY: #{@search_json}"


    respond_to do |format|
     format.html
     format.xlsx
     format.csv { send_data @roles.to_csv}
   end
  end

  # GET /roles/1
  # GET /roles/1.json
  def show
  end

  def enable_role
      role_id=params[:id]
     if Role.update(role_id, :status => 1)
       redirect_to roles_path, notice: 'Role was successfully enabled.'
     end
  end

  def disable_role
      role_id=params[:id]
     if Role.update(role_id, :status => 0)
       redirect_to roles_path, notice: 'Role was successfully disabled.'
     end
  end

  # GET /roles/new
  def new
    @role = Role.new
  end

  # GET /roles/1/edit
  def edit
  end

  # POST /roles
  # POST /roles.json
  def create
    @role = Role.new(role_params)

    respond_to do |format|
      if @role.save
        format.html { redirect_to @role, notice: 'Role was successfully created.' }
        format.json { render :show, status: :created, location: @role }
      else
        format.html { render :new }
        format.json { render json: @role.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /roles/1
  # PATCH/PUT /roles/1.json
  def update
    respond_to do |format|
      if @role.update(role_params)
        format.html { redirect_to @role, notice: 'Role was successfully updated.' }
        format.json { render :show, status: :ok, location: @role }
      else
        format.html { render :edit }
        format.json { render json: @role.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /roles/1
  # DELETE /roles/1.json
  def destroy
    @role.destroy
    respond_to do |format|
      format.html { redirect_to roles_url, notice: 'Role was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_role
      @role = Role.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def role_params
      params.require(:role).permit(:role_name, :status)
    end
end
