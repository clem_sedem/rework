class CreateLocalities < ActiveRecord::Migration
  def change
    create_table :localities do |t|
      t.integer :suburb_id
      t.string :locality_name

      t.timestamps null: false
    end
  end
end
