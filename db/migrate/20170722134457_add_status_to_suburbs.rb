class AddStatusToSuburbs < ActiveRecord::Migration
  def change
    add_column :suburbs, :status, :boolean
  end
end
