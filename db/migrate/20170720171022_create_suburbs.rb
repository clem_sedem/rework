class CreateSuburbs < ActiveRecord::Migration
  def change
    create_table :suburbs do |t|
      t.integer :city_id
      t.string :suburb_name

      t.timestamps null: false
    end
  end
end
