class AddStatusToRegions < ActiveRecord::Migration
  def change
    add_column :regions, :status, :boolean
  end
end
