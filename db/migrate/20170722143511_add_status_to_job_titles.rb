class AddStatusToJobTitles < ActiveRecord::Migration
  def change
    add_column :job_titles, :status, :boolean
  end
end
