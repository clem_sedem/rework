class AddStatusToCities < ActiveRecord::Migration
  def change
    add_column :cities, :status, :boolean
  end
end
