class CreateContacts < ActiveRecord::Migration
  def change
    create_table :contacts do |t|
      t.string :firstname
      t.string :lastname
      t.string :phone_number
      t.string :email
      t.string :gender
      t.integer :locality_id
      t.integer :job_title_id

      t.timestamps null: false
    end
  end
end
