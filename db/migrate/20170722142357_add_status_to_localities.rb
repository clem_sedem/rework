class AddStatusToLocalities < ActiveRecord::Migration
  def change
    add_column :localities, :status, :boolean
  end
end
